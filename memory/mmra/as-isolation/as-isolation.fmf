summary: Test memory access between userspace processes.
description: |
    Checks that user processes can't write to others' private memory segments.
    The program will map 90% of total RAM across a fixed number of forked
    processes, and each one of these processes will concurrently write an
    unique byte pattern to its private mapped chunk of address space.
    After all processes have finish writing to their address space mapped
    chunk, they will then concurrently read back the map's contents
    asserting that previously written byte pattern.
    Test input:
        A program, as-isolation, that will map 90% of the total RAM.
        Input details:
        - as-isolation.c, source.
        - /tmp/as-isolation
    Expected results:
        If the read back operation of the patterns wrote to memory segments is
        successful (meaning they didn't interfere with each other) you should
        expect the following results:
        [   PASS   ] :: Command 'gcc -o /tmp/as-isolation -D_GNU_SOURCE -lpthread as-isolation.c' (Expected 0, got 0)
        [   PASS   ] :: Command '/tmp/as-isolation' (Expected 0, got 0)
    Results location:
        output.txt
contact: Rafael Aquini <aquini@redhat.com>
test: |
    if [ -n "${FFI_QM_SCENARIO}" ]; then
        podman exec --env TEST_DIR="$(pwd)" --env BEAKERLIB_DIR="$BEAKERLIB_DIR" \
          -t qm sh -c 'cd "$TEST_DIR"; bash runtest.sh'
    else
        bash ./runtest.sh
    fi
framework: beakerlib
require:
  - beakerlib
  - gcc
  - glibc-devel
extra-summary: memory/mmra/as-isolation
extra-task: memory/mmra/as-isolation
id: 108e0b46-7840-4486-9a6b-c6fe3a495489
enabled: true
duration: 1h
