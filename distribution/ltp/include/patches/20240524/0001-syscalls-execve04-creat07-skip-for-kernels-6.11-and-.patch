From 12588eee7bd3160d2492944be26aa69cb3d7cf6d Mon Sep 17 00:00:00 2001
From: Jan Stancek <jstancek@redhat.com>
Date: Mon, 12 Aug 2024 08:52:05 +0200
Subject: [PATCH] syscalls/execve04,creat07: skip for kernels 6.11 and later

The behaviour is allowed since commit:
  2a010c412853 ("fs: don't block i_writecount during exec")
that landed in 6.11-rc1.

Closes: https://github.com/linux-test-project/ltp/issues/1184

Reported-by: kernel test robot <oliver.sang@intel.com>
Signed-off-by: Jan Stancek <jstancek@redhat.com>
Reviewed-by: Petr Vorel <pvorel@suse.cz>
Reviewed-by: Li Wang <liwang@redhat.com>
---
 testcases/kernel/syscalls/creat/creat07.c   | 10 ++++++++++
 testcases/kernel/syscalls/execve/execve04.c | 10 ++++++++++
 2 files changed, 20 insertions(+)

diff --git a/testcases/kernel/syscalls/creat/creat07.c b/testcases/kernel/syscalls/creat/creat07.c
index 7bd32ab4d..f157e1a8f 100644
--- a/testcases/kernel/syscalls/creat/creat07.c
+++ b/testcases/kernel/syscalls/creat/creat07.c
@@ -47,7 +47,17 @@ static void verify_creat(void)
 	SAFE_WAITPID(pid, NULL, 0);
 }
 
+static void setup(void)
+{
+	if ((tst_kvercmp(6, 11, 0)) >= 0) {
+		tst_brk(TCONF, "Skipping test, write to executed file is "
+			"allowed since 6.11-rc1.\n"
+			"2a010c412853 (\"fs: don't block i_writecount during exec\")");
+	}
+}
+
 static struct tst_test test = {
+	.setup = setup,
 	.test_all = verify_creat,
 	.needs_checkpoints = 1,
 	.forks_child = 1,
diff --git a/testcases/kernel/syscalls/execve/execve04.c b/testcases/kernel/syscalls/execve/execve04.c
index 3bac642e5..7bbfece85 100644
--- a/testcases/kernel/syscalls/execve/execve04.c
+++ b/testcases/kernel/syscalls/execve/execve04.c
@@ -65,7 +65,17 @@ static void do_child(void)
 	exit(0);
 }
 
+static void setup(void)
+{
+	if ((tst_kvercmp(6, 11, 0)) >= 0) {
+		tst_brk(TCONF, "Skipping test, write to executed file is "
+			"allowed since 6.11-rc1.\n"
+			"2a010c412853 (\"fs: don't block i_writecount during exec\")");
+	}
+}
+
 static struct tst_test test = {
+	.setup = setup,
 	.test_all = verify_execve,
 	.forks_child = 1,
 	.child_needs_reinit = 1,
-- 
2.46.0

